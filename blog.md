# Batch Update in React

## What is batch update?
Batching in React when React groups the multiple change is state are grouped into single update for better performance.

Here in the given example we are changing the state 3 times but the render will happen only once.

```import React from "react";
class App extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            name: '',
            likes: '',
            product: ''
        };
    }
    handleClick = () => {
        this.setState({
            name:"john",
        })
        this.setState({
            likes:"cricket",
        })
        this.setState({
            product:"bat"
        })

    }

    render() {
        console.log("rendering");

        return (
            <div className="App">
                <div>
                    <button onClick={this.handleClick}>Next</button>
                </div>
            </div>
        );
    }
}
export default App;```

